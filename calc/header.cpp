/*
    Pizm: display the first 15000 digits (following the decimal point) of π
    Copyright (C) 2014  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <fstream>
using namespace std;

int main (int argc, char* argv[]) {

	ifstream inputf(argv[1]);
	if (!inputf.is_open()){
		cout << "Nem sikerült megnyitni a megadott input file-t: " << argv[1] << "\n";
		return 1;
	}
	
	ofstream outputf((argc >= 3) ? argv[2] : "pi.h");
	if (!outputf.is_open()){
		cout << "Nem sikerült megnyitni a megadott output file-t!\n";
		inputf.close();
		return 1;
	}
	
	inputf.seekg(0, inputf.end);
    unsigned int length = inputf.tellg()/2;
    unsigned int digits = length*2;
    inputf.seekg(0, inputf.beg);
    
    outputf << "#ifndef H_PI_SZAM\n#define H_PI_SZAM\n\nunsigned const int pi_length = " << length <<";\nunsigned const int pi_digits = " << digits << ";\n\nunsigned const char pi["<< length <<"] = {";
	
	char c[2];
	unsigned char o;
	unsigned int i;
	for(i=0; i<length-1; i++){
		c[0] = inputf.get() - '0';
		c[1] = inputf.get() - '0';
		o = c[0];
		o <<= 4;
		o |= c[1];
		outputf << (int)o << ", ";
	}
	c[0] = inputf.get() - '0';
	c[1] = inputf.get() - '0';
	o = c[0];
	o <<= 4;
	o |= c[1];
	outputf << (int)o << "};\n\n#endif";
	
	inputf.close();
	outputf.close();

	return 0;
}
