/*
    Pizm: display the first 15000 digits (following the decimal point) of π
    Copyright (C) 2014  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


const int help_sorok_szama=54;
const char help_text[] = "       Pi(zm) 1.00\n\n  This little plug-in can\ndisplay the first 15000\ndigits (following the\ndecimal point) of the\nnumber pi. You can use it\njust for fun but this\nprogram can also help you\nby learning the digits.\nCurrently 15000 digits\nare stored but if you\nneed more (I can't imagi-\nne why...) just write me\nand I will add more di-\ngits. It's important that\ndigits are stored and not\ncomputed, future releases\nmight include this fea-\nture.\n\n\n    *** Controls ***\n\n  Use UP and DOWN arrow\nkeys to scroll and press\narrow keys LEFT and RIGHT\nor [F2] and [F3] to skip\nto the previous/next page\n  Press [F1] to jump to a\ngiven position. After en-\ntering a number between 1\nand 15000, press [EXE].\nThe selected digit will\nbe highlighted.\n  Press [F5] to enter\nPlay mode.\n  Press [F6] to display\nthe help page.\n\n\n     *** Credits ***\n\nDeveloped by Balping\nbalping.official@gmail.com\n(C) balping 2014\n\nThis package is\ndownloadable at\ncemetech.net\nhttp://tiny.cc/aius9w\n\nSupport:\nhttp://tiny.cc/znus9w";
