/*
    Pizm: display the first 15000 digits (following the decimal point) of π
    Copyright (C) 2014  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "include.h"
#include "help.h"
#include "pi.h"
#include "szam.h"
#include "play.h";

int main(void) {
	EnableStatusArea(0);
	DefineStatusAreaFlags(DSA_SETDEFAULT, 0, 0, 0);
	DefineStatusAreaFlags(3, SAF_BATTERY | SAF_TEXT | SAF_GLYPH | SAF_ALPHA_SHIFT, 0, 0);
	init_fkey();
	
	
	int i = -1;
	int highlight=-1;
	int key;
	while (1) {
		display_pi(i, highlight);
		ctrl(&i, &highlight);
		if(i<(-1)){i=-1;}
		if(i>=(signed int)(pi_digits/20)){i=pi_digits/20-7;}
		if(i>(signed int)(pi_digits/20-7)){funny_message(&i, &highlight); i=pi_digits/20-7;}
	}
	return 0;
}

int jump(int i, int * reali, char * message){
	int ie=i;
	if(i<0){i=0;}
	i=20*i+1;
	MsgBoxPush(2);
	int len=5;//15000
	int start = 0;
	int cursor = 0;
	int x=13;
	int y=4;
	PrintXY(3, 3, message, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	int y_px=(y-1)*24;
	int maxpos = x+len-1;
	char buffer[len+1];
	itoa(i, buffer);
	int running=1;
	char *c;
	int key;
	cursor = strlen(buffer);
	if(cursor==len){key=KEY_CTRL_RIGHT;EditMBStringCtrl2(buffer, len+1, &start, &cursor, &key, x, y_px, 1, maxpos);}else{
	DisplayMBString2(0, buffer, start, cursor, 0, x, y_px, maxpos, 0, 0);}
	while(running){
		GetKey(&key);

		if(key == KEY_CTRL_EXE){
			running=0;
			c=buffer;
			i=0;
			while(*c){
				i=i*10+((*c)-'0');
				c++;
			}
			itoa(i, buffer);
		}else if(key == KEY_CTRL_EXIT){
			running=0;
			*reali=-10;
			Cursor_SetFlashOff();
			MsgBoxPop();
			return ie;
		}

		if(key && key < 30000){
			if(key>=KEY_CHAR_0 && key<=KEY_CHAR_9){
				cursor = EditMBStringChar(buffer, len, cursor, key);
				DisplayMBString2(0, buffer, start, cursor, 0, x, y_px, maxpos+1, 0, key);
			}
		}else{
			if(key!=KEY_CTRL_PASTE){
				//EditMBStringCtrl((unsigned char*)buffer, 4, &start, &cursor, &key, 3, 3);
				EditMBStringCtrl2(buffer, len+1, &start, &cursor, &key, x, y_px, 1, maxpos);
				if(GetSetupSetting(0x14)==2){SetSetupSetting(0x14,0);}//clip elkerülése
			}
		}
	}
	Cursor_SetFlashOff();
	MsgBoxPop();
	if(i<=0){
		*reali = -1;
		return -1;
	}else{
		*reali=i;
		i = (i-1)/20;
		if(i>(signed int)(pi_digits/20-7)){ i=pi_digits/20-7;}
		return i;
	}
}

void get_digits(char * ebbe, int start, int len){
	int i;
	for(i=start; i<start+len; i++){
		ebbe[i-start] = get_digit(i);
	}
	ebbe[i-start] = 0;
}

char get_digit(int i){
	if(i>=0 && i<pi_digits){
		return ((pi[i/2] >> 4*((i+1)%2))&15)+'0';
	}else{
		switch(i){
			case -2:
				return '3';
				break;
			case -1:
				return '.';
				break;
			default:
				return ' ';
				break;
		}
	}
}

void display_pi(int start, int highlight){
	char buff[23];
	int kezdosor = 1;

	if(start<=-1){
		kezdosor = 2;
		start = 0;
		PrintXY(1, 1, "xx               \xE6\x4F \xE6\xB1 3.", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	}
	
	if(start>(pi_digits/20-7)){start = pi_digits/20-7;}
	
	int i;
	for(i=start; kezdosor+i-start<=7&&i<pi_digits/20; i++){
		get_digits(buff+2, i*20, 10);
		buff[12]=' ';
		get_digits(buff+13, i*20+10, 10);
		PrintXY(1, kezdosor+i-start, buff, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	}

	if(highlight>=start*20+1 && highlight<=i*20){
		highlight--;
		get_digits(buff+2, highlight, 1);
		PrintXY(highlight%20+((highlight%20>=10)?2:1), (highlight/20)-start+kezdosor, buff, TEXT_MODE_INVERT, TEXT_COLOR_BLACK);
	}
	
	buff[0] = 'P'; buff[1] = 'i'; buff[2] = ' '; buff[3] = '(';
	itoa((start*20+1), buff+4);
	int sl = strlen(buff);
	buff[sl] = '-';
	itoa(i*20, buff+sl+1);
	sl = strlen(buff);
	buff[sl] = ')'; buff[sl+1]=0;
	DefineStatusMessage(buff,1,0,0);
	DisplayStatusArea();
}

void funny_message(int * i, int * highlight){
	
	
	/*while (1) {
		display_pi(*i, *highlight);
		ctrl(i, highlight);
		
	}
	if(*i>(signed int)(pi_digits/20-6)){
		PrintXY(1, 6, "xxSorry, there are no", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXY(1, 7, "xxmore digits stored :(", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	}*/
}

void ctrl(int * i, int * highlight){
	int key;
	GetKey(&key);
	switch (key) {
		case KEY_CTRL_UP:
			(*i)--;
			break;
		case KEY_CTRL_DOWN:
			(*i)++;
			break;
		case KEY_CTRL_RIGHT:
		case KEY_CTRL_F3:
			(*i)+=7;
			break;
		case KEY_CTRL_LEFT:
		case KEY_CTRL_F2:
			(*i)-=7; 
			break;
		case KEY_CTRL_F4:
			(*i)=-1;
			break;
		case KEY_CTRL_F1:
			(*i)=jump(*i, highlight, "xxJump to digit:");
			break;
		case KEY_CTRL_EXIT:
			highlight=-1;
			break;
		case KEY_CTRL_F5:
			play();
			break;
		case KEY_CTRL_F6:
			help();
			break;
	}
}

void init_fkey(){
	int result;
	GetFKeyPtr(0x01FC, &result);//JUMP
	FKey_Display(0, (void*)result );
	GetFKeyPtr(0x0437, &result);//PageUp
	FKey_Display(1, (void*)result );
	GetFKeyPtr(0x0438, &result);//PageDown
	FKey_Display(2, (void*)result );
	GetFKeyPtr(0x0189, (void*)&result);//Top
	FKey_Display(3, (void*)result );
	GetFKeyPtr(0x0404, &result);//Play
	FKey_Display(4, result );
	GetFKeyPtr( 0x03FD, &result );//HELP
	FKey_Display(5, (void*)result );
}
