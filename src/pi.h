/*
    Pizm: display the first 15000 digits (following the decimal point) of π
    Copyright (C) 2014  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

int jump(int i, int * reali, char * message);
void funny_message(int * i, int * highlight);
char get_digit(int i);
void get_digits(char * ebbe, int start, int len);
void display_pi(int start, int highlight);
void init_fkey();
void ctrl(int * i, int * highlight);
