/*
    Pizm: display the first 15000 digits (following the decimal point) of π
    Copyright (C) 2014  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "include.h"


#define status_print() \
sys_strcpy(buff, "Pi - Playing (");\
sl = strlen(buff);\
itoa(kezd+1, buff+sl);\
sl = strlen(buff);\
buff[sl] = '-';\
itoa(i+1, buff+sl+1);\
sl = strlen(buff);\
buff[sl] = ')'; buff[sl+1]=0;\
DefineStatusMessage(buff,1,0,0);\
DisplayStatusArea()

void play();
int wait_for_key(int mire, int vagymire){
	int key;
	do{
		GetKey(&key);
	}while(key!=mire && key!=vagymire);
	return key;
}

void play(){
	Bdisp_AllClr_VRAM();
	DefineStatusMessage("Pi - Playing",1,0,0);
	DisplayStatusArea();
	
	int result;
	GetFKeyPtr( 0x03FD, &result );//HELP
	FKey_Display(5, (void*)result );
	
	int kezd;
	jump(-24, &kezd, "xxStart from digit:");
	if(kezd==-10){
		goto vege;
	}
	
	if(kezd<1 || kezd>pi_digits){kezd=1;}
	kezd--;
	char buff[23];
	char scoreb[23];
	int score=0;
	
	
	locate_OS(16, 5);
	Cursor_SetFlashOn(0);
	
	
	GetFKeyPtr(0x0409, &result);//<<
	FKey_Display(0, result );
	
	PrintXY(3, 3, "xxDigits:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	
	int key;
	char mehet=1;
	char rossz = 0;//beütett számjegy rossz-e
	char be;
	int i = kezd;
	int sl;
	status_print();
	while(mehet){
		get_digits(buff+2, i-15, 15);
		PrintXY(1, 5, buff, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		itoa(score, scoreb+2);
		PrintXY(3, 2, "xxScore:               ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXY(11, 2, scoreb, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		itoa(i-kezd, buff+2);
		PrintXY(11, 3, buff, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		
		if(rossz){
			Cursor_SetFlashOn(7);
		}
		
		GetKey(&key);
		switch (key) {
			case KEY_CHAR_0:
			case KEY_CHAR_1:
			case KEY_CHAR_2:
			case KEY_CHAR_3:
			case KEY_CHAR_4:
			case KEY_CHAR_5:
			case KEY_CHAR_6:
			case KEY_CHAR_7:
			case KEY_CHAR_8:
			case KEY_CHAR_9:
				be = key-KEY_CHAR_0;
				if(be+'0'==get_digit(i)){
					PrintXY(16, 5, "xx ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
					status_print();
					i++;
					if(!rossz){
						score++;
					}else{
						rossz=0;
						PrintXY(1, 7, "xx                     ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
					}
					if(i>=pi_digits){
						mehet=0;
						Cursor_SetFlashOff();
						MsgBoxPush(3);
						PrintXY(3, 3, "xxThere are no more", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
						PrintXY(3, 4, "xxdigits stored.", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
						PrintXY(3, 5, "xxSorry :-(", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
						wait_for_key(KEY_CTRL_EXIT, KEY_CTRL_F1);
						MsgBoxPop();
						GetFKeyPtr(0, &result );
						FKey_Display(5, (void*)result );
						wait_for_key(KEY_CTRL_EXIT, KEY_CTRL_F1);
					}
				}else{
					buff[2] = be+'0'; buff[3] = 0;
					PrintXY(16, 5, buff, TEXT_MODE_NORMAL, TEXT_COLOR_RED);
					if(!rossz){PrintXY(1, 7, "xxPress EXE to get help", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);}
					rossz=1;
				}
				break;
			case KEY_CTRL_EXE:
					get_digits(buff+2, (i/10)*10, 10);
					buff[12]=' ';
					get_digits(buff+13, (i/10)*10+10, 10);
					PrintXY(1, 7, buff, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
					get_digits(buff+2, i, 1);
					PrintXY(1+i%10, 7, buff, TEXT_MODE_INVERT, TEXT_COLOR_BLACK);
					if(rossz<2){score--;}
					rossz=2;
				break;
			case KEY_CTRL_F1:
			case KEY_CTRL_EXIT:
				mehet=0;
				break;
			case KEY_CTRL_F6:
				Cursor_SetFlashOff();
				help();
				Cursor_SetFlashOn(0);
				break;
		}
	}
	
	
vege:
	Cursor_SetFlashOff();
	Bdisp_AllClr_VRAM();
	init_fkey();
}
